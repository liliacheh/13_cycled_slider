let images = document.querySelectorAll("img")
images.forEach(elem => elem.classList.add("hidden"))
let i = 0;
let timer;
let start = document.querySelector(".start")
let pause = document.querySelector(".pause")
start.disabled = "false"
function showImg(){
    if (i < images.length-1){
        images[i].classList.toggle("hidden")}
        if(i >= 1 ){
        images[i-1].classList.add("hidden")}
        if (i == images.length-1){
            images[i].classList.remove("hidden")  
        }
        if (i>images.length-1){
            i=0
            images[0].classList.toggle("hidden")
        }
        i++
        timer = setTimeout(showImg,3000)
}
timer = setTimeout(showImg,0)

 setTimeout(function showBtn(){
    let btns = document.querySelectorAll("button")
    btns.forEach(elem => elem.removeAttribute("hidden"))  
 }, 3000)

function pauseImg() {
    clearTimeout(timer);
    start.disabled = false;
    pause.disabled = true;    
 }
pause.addEventListener("click",pauseImg)

function startImg() {
    timer = setTimeout(showImg,3000);
    start.disabled = true;
    pause.disabled = false;
 } 
 start.addEventListener("click",startImg)